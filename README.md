# tls-certificates 🔐

## Considerations

### Encryption Type

RSA and ECC are both widely supported, particularly by browsers that support HTTP/2. [^symantec]

ECC is more efficient in CPU and bandwidth usage. At this time RSA is still widely used but CloudFlare popularised ECC. [^cloudflare]

### ECC Curves

*Use `secp384r1` (P-384) for now. Consider switching to `x25519` (Curve25519) once Safari and Edge support it.*

NSA Suite B was a recommendation published by NIST with two curves for ECC, namely P-256 and P-384. In OpenSSL these are called `secp256r1` and `secp384r1`. Later this was amended to recommend only `P-384` due to concerns about quantum computer development. [^iad]

`secp256r1` and `secp384r1` are supported by Firefox, Chrome, and Safari. [^ssllabs]

`secp521r1` curve support was dropped by Google Chrome. [^P-521] Firefox and Safari still support it.

Some concerns and controversy exists around the security of the recommended curves. [^snowden]

IETF is standardising replacements called Curve25519 (~128-bit security level) and Curve448 (~224-bit security level). [^RFC7748] Curve25519 is supported since Chrome v50. [^Curve25519] Also supported by Firefox v5x. [^firefox] Not yet supported by Safari [^safari] or Edge [^edge].

## Generating

Create a private key. This key is used by the web server and must be kept securely. Never share with anyone.

```text
openssl ecparam -out wildcard.http2.live.pem -name secp384r1 -genkey
```

Create a Certificate Signing Request (CSR). This is submitted to the Certificate Authority (CA).

```text
openssl req -new -key wildcard.http2.live.pem -nodes -sha256 -out wildcard.http2.live.csr -subj "/C=SG/ST=Singapore/L=Singapore/O=Sebastiaan Deckers/CN=*.http2.live"
```

Note: The CSR should specify the bare domain and wildcard domain as values in the *Subject Alternative Name* extension field. This is rather tricky to do with OpenSSL, involving a config file. AlphaSSL/GlobalSign inject the SAN automatically into the signed certificate so we don't need to bother.

## Choosing a CA

For wildcard certificates the cheapest option seems to be AlphaSSL as resold by SSL2Buy. [^wildcard]

Note that AlphaSSL is merely a proxy to GlobalSign who do the actual certificate signing. [^alphassl]

## Domain Control Validation

The CA requires domain owners to prove they control the domain.

GlobalSign supports either email or URL verification. Either set up email at `[admin|administrator|hostmaster|postmaster|webmaster]@http2.live` or publish a file containing the required `META` tag.

### Domain Validation Code (DVC)

```html
<meta name="_globalsign-domain-verification" content="3KCckju8CtIX8n_XlqyGXeomwdosyNFKY2D3sr2DH-" />
```

### URL

Choose either of:

- <http://http2.live/.well-known/pki-validation/gsdv.txt>
- <https://http2.live/.well-known/pki-validation/gsdv.txt>

### Deployment

Use GitLab Pages, see `.gitlab-ci.yml`. Add the domain to this repository's settings. Add `http2.live` CNAME to `sebdeckers.gitlab.io` usingf CloudFlare. This is only necessary for the initial certificate signing. Renewal will happen using the actual web hosting service once it goes live.

## References

[^symantec]: http://csrc.nist.gov/groups/ST/ecc-workshop-2015/presentations/session2-andrews-rick.pdf
[^cloudflare]: https://blog.cloudflare.com/a-relatively-easy-to-understand-primer-on-elliptic-curve-cryptography/
[^iad]: https://www.iad.gov/iad/library/ia-guidance/ia-solutions-for-classified/algorithm-guidance/commercial-national-security-algorithm-suite-factsheet.cfm
[^ssllabs]: https://www.ssllabs.com/ssltest/viewMyClient.html
[^snowden]: https://crypto.stackexchange.com/questions/10263/should-we-trust-the-nist-recommended-ecc-parameters
[^P-521]: https://bugs.chromium.org/p/chromium/issues/detail?id=478225
[^RFC7748]: https://tools.ietf.org/html/rfc7748
[^Curve25519]: https://www.chromestatus.com/features/5682529109540864
[^firefox]: https://bugzilla.mozilla.org/show_bug.cgi?id=957105
[^safari]: https://www.ssllabs.com/ssltest/viewClient.html?name=Safari&version=10&platform=OS%20X%2010.12&key=138
[^edge]: https://www.ssllabs.com/ssltest/viewClient.html?name=Edge&version=13&platform=Win%2010&key=130
[^wildcard]: https://serverfault.com/questions/599219/how-to-decide-where-to-purchase-a-wildcard-ssl-certificate
[^alphassl]: https://www.alphassl.com/about.html
